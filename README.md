# Chapter 06 Challenge, FSW 5, Irfan Nada Bayu Samudera

# Team:

Leader: Jovan Alvado\
Members: Irfan Nada Bayu Samudera, Bagas Timur Nurcahyo, Muhammad Bilal Athallah.

# Techs:

Node.js, Express.js, MariaDB (MySQL), Sequelize ORM, JWT Auth, etc.

# APIs:

You can test the APIs in the Postman, you can import Challenge06_APIs.postman_collection to your Postman or click link below:
APIs documentation online open: https://documenter.getpostman.com/view/18883155/Uyxhmn3k

Step from your postman:

1. Import the file mentioned above (postman collection) into your post man
2. Run "npm install nodemon"
3. Run your web server and Mysql database (example: using xampp to run Apache web server and Mysql for database)
4. Run "sequelize db:migrate"
5. Run "sequelize db:seed:all
6. Run "npm run dev"
7. Happy playing with the APIs from Postman!

# Features:

1. Superadmin:\
   1.1. Register Admin\
   1.2. Check user by token\
   1.3. Create Read Update Delete data Car
2. Admin:\
   2.1. Check user by token\
   2.2. Create Read Update Delete data Car
3. Member:\
   3.1. Read Car data (All or by spesific ID)
4. Guest:\
   4.1. Register as member

# License

Please cite the source, however use it at your own risk and we are not responsible for any damage

# Thanks for your visit!
