"use strict";
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { DATE } = require("sequelize");

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert("cars", [
      {
        UserId: 1,
        plate: "B1234XC",
        manufacture: "TESLA",
        model: "Cyber Truck",
        image: "Cybertruck.jpg",
        rentperday: 1000000,
        capacity: 8,
        description: "Brand New Electric Car by TESLA",
        transmission: "M",
        type: "Light-Duty Truck",
        year: "2019",
        options: "Auto Pilot: Yes",
        specs: "EPA Range: 400-800 KM",
        availableAt: "2020-05-13 03:12:31",
        createdAt: new Date(),
        updatedAt: new Date(),
        createdBy: "superadmin",
        updatedBy: "superadmin",
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("users", {
      [Op.or]: [{ id: 1 }, { id: 2 }],
    });
  },
};
