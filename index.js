const express = require('express')
const app = express()
const userRouter = require('./routes/user')
const carRouter = require('./routes/car')
const dotenv = require('dotenv')
const cookieParser = require('cookie-parser')
const cors = require("cors");

dotenv.config()

app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(cors());

app.use('/api/v1', userRouter)
app.use('/api/v1', carRouter)

app.listen(5000, () => {
    console.log(`Server running on http://localhost:5000`);
})