const express = require("express");
const handler = require("../controllers/car");
const carRouter = express.Router();
const verifyToken = require('../middleware/verifyToken');
const verifyAdmin = require('../middleware/verifyAdmin')

carRouter.get("/cars", verifyToken, handler.getAllData);
carRouter.get("/car/:id", verifyToken, handler.getById);
carRouter.post("/car", verifyAdmin, handler.createData);
carRouter.put("/car/:id", verifyAdmin, handler.updateData);
carRouter.delete("/car/:id", verifyAdmin, handler.deleteData);

module.exports = carRouter;