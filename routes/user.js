const express = require("express");
const handler = require("../controllers/user");
const userRouter = express.Router();
const refreshToken = require("../controllers/refreshToken");
const verifyAdmin = require("../middleware/verifyAdmin");
const verifySU = require("../middleware/verifySU");
const verifyToken = require("../middleware/verifyToken");

userRouter.get("/users", verifyAdmin, handler.getAllUser);
userRouter.get("/user/get", handler.getUserByToken);
userRouter.post("/user/create", handler.registerUser);
userRouter.post("/admin/create", verifySU, handler.registerAdmin);
userRouter.post("/user/login", handler.loginUser);
userRouter.get("/user/token", refreshToken);
userRouter.delete("/user/logout", verifyToken, handler.logout);

module.exports = userRouter;
